<?php

namespace App\Http\Controllers;

use App\Imports\CompanyImport;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class CompanyController extends Controller
{
    /**
     * The default pagination size.
     * 
     * @var integer
     */
    private const PAGINATION_DEFAULT = 10; 

    /**
     * The error bag.
     * 
     * @var string
     */
    private const ERROR_BAG = 'company';

    /**
     * The validation rules.
     * 
     * @var array
     */
    private const VALIDATION_RULES = [
        'business_name' => ['required', 'string'],
        'address'       => ['required', 'string'],
        'zipcode'       => ['required', 'postal_code:IT,US'],
        'city'          => ['required', 'string'],
        'province'      => ['required', 'string'],
        'region'        => ['required', 'string'],
        'email'         => ['required', 'email'],
    ];

    /**
     * Renders the paginated list of companies.
     */
    public function index(Request $request)
    {
        $this->authorize('company_access');

        $search_filter = '%' . $request['search'] . '%';
        $pagination = Company::where('business_name', 'LIKE', $search_filter)
                        ->orWhere('email', 'LIKE', $search_filter)
                        ->paginate(self::PAGINATION_DEFAULT);

        $pagination->appends($request->all());

        return Inertia::render('Company/Index', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Renders the company info the given id.
     */
    public function show(Request $request, $id) {
        $this->authorize('company_show');

        $company = Company::find($id);
        
        if (!$company) {
            return abort(404);
        }

        return Inertia::render('Company/Show', [
            'company' => $company
        ]);
    }

    /**
     * Renders the create page to create a new company.
     */
    public function create(Request $request) {
        $this->authorize('company_create');
        return Inertia::render('Company/Create');
    }

    /**
     * Validates and stores a new company.
     */
    public function store(Request $request) {
        $this->authorize('company_create');

        // validate the data
        $validated = $request->validateWithBag(self::ERROR_BAG, self::VALIDATION_RULES);

        // create the new company
        $company = Company::create($validated);
        
        return redirect()->route('company.show', [
            'id' => $company->id
        ]);
    }

    /**
     * Renders the edit of a comany from the given id.
     */
    public function edit(Request $request, $id) {
        $this->authorize('company_edit');

        $company = Company::find($id);
        
        if (!$company) {
            return abort(404);
        }
        
        return Inertia::render('Company/Edit', [
            'company' => Company::find($id)
        ]);
    }

    /**
     * Validates and updates a company's data from the given id.
     */
    public function update(Request $request, $id) {
        $this->authorize('company_edit');

        $company = Company::find($id);
        
        // check if the company exists
        if (!$company) {
            return abort(404);
        }

        // validate the data
        $validated = $request->validateWithBag(self::ERROR_BAG, self::VALIDATION_RULES);

        // upate the company
        $company->update($validated);

        return redirect()->back();
    }

    /**
     * Deletes a company from the given id.
     */
    public function destroy(Request $request, $id) {
        $this->authorize('company_delete');

        $company = Company::find($id);
        
        // check if the company exists
        if (!$company) {
            return abort(404);
        }

        $company->delete();

        return redirect()->route('company.index');
    }

    public function import(Request $request) {
        $this->authorize('company_import');
        return Inertia::render('Company/Import');
    }

    public function storeImport(Request $request) {
        $this->authorize('company_import');

        $validated = $request->validateWithBag('importData', [
            'file' => ['required', 'mimes:xlsx']
        ]);

        Excel::import(new CompanyImport, $validated['file']);

        return redirect()->back();
    }
}
