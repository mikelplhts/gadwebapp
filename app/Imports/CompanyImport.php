<?php

namespace App\Imports;

use App\Models\Company;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;

class CompanyImport implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Company([
            'business_name' => $row['business_name'],
            'address'       => $row['address'],
            'zipcode'       => $row['zipcode'],
            'city'          => $row['city'],
            'province'      => $row['province'],
            'region'        => $row['region'],
            'email'         => $row['email'],
        ]);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'business_name' => ['required', 'string'],
            'address' => ['required', 'string'],
            'zipcode' => ['required', 'postal_code:IT,US'],
            'city' => ['required', 'string'],
            'province' => ['required', 'string'],
            'region' => ['required', 'string'],
            'email' => ['required', 'email'],
        ];
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // skipping failures
        Log::debug($failures);
    }

    /**
     * @param Throwable $e
     */
    public function onError(Throwable $e)
    {
        // skipping errors
        Log::debug($e);
    }
}
