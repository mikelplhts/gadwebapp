<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * The array of permission names.
     * 
     * @var string[]
     */
    private $permissions = [
        'company_access',
        'company_create',
        'company_edit',
        'company_show',
        'company_delete',
        'company_import',
    ];

    /**
     * The user permissions.
     * 
     * @var string[]
     */
    private $userPermissions = [];



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        foreach ($this->permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // admin role
        $adminRole = Role::create(['name' => 'admin']);
        $adminRole->givePermissionTo($this->permissions);

        // user role
        $userRole = Role::create(['name' => 'user']);
        $userRole->givePermissionTo($this->userPermissions);
    }
}
