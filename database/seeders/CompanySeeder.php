<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * The number of companies to create.
     */
    private const NUM_COMPANIES = 100;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $each = 100;

        for ($i = 0; $i < self::NUM_COMPANIES/$each; $i++) {
            Company::factory($each)->create();
        }
    }
}
