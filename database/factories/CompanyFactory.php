<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'business_name' => $this->faker->company(),
            'address' => $this->faker->streetSuffix() . ' ' . $this->faker->streetAddress() . ' ' . $this->faker->buildingNumber(),
            'zipcode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'province' => $this->faker->city(),
            'region' => $this->faker->city(),
            'email' => $this->faker->unique()->email()
        ];
    }
}
