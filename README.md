# GadWebApp

The test project for [GAD](https://gadstudio.eu). It requires six levels:

1. Login/Signup (required) (done)
2. Company details CRUD (required) (done)
3. Data test creation (required) (done)
4. Data import from Excel (optional) (done)
5. Roles and permissions (optional) (done)
6. CRUD tests (optional)

## Installation

First of all, clone the project

```sh
git clone git@gitlab.com:mikelplhts/gadwebapp.git GadWebApp
cd GadWebApp/
```

Then install the packages using npm

```sh
npm install && npm run dev
```

Setup the environment file by using `.env.example` as template

```sh
cp .env.example .env

# configure the file
vim .env
```

Migrate the project

```sh
php artisan migrate
```

Now it is necessary to seed the roles and permissions by executing

```sh
php artisan db:seed PermissionSeeder
```

Finally serve:

```sh
php artisan sarve
```

For development, it's also necessary to run

```sh
npm run watch
```

This generates the CSS and JS files.