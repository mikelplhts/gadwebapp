<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class ImportCompanyTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_role_cannot_import_companies() {
        $user = User::factory()->create();
        $this->actingAs($user);

        Excel::fake();
        
        $file = UploadedFile::fake()->create('myexcel.xlsx');
        $response = $this->post('/dashboard/companies/import', [
            'file' => $file
        ]);

        $response->assertStatus(403);
    }

    public function test_admin_role_can_import_companies() {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        Excel::fake();
        
        $file = UploadedFile::fake()->create('myexcel.xlsx');
        $response = $this->post('/dashboard/companies/import', [
            'file' => $file
        ]);

        $response->assertStatus(302);
    }
}
