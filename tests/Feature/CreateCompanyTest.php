<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class CreateCompanyTest extends TestCase
{
    use RefreshDatabase;

    private array $testData = [
        'business_name' => 'Test Company',
        'address'       => 'Street Test 123',
        'zipcode'       => '12345',
        'city'          => 'Test City',
        'province'      => 'Test Province',
        'region'        => 'Test Region',
        'email'         => 'example@email.com'
    ];

    public function test_user_role_cannot_create_new_companies()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->post('/dashboard/companies', $this->testData);
        $response->assertStatus(403);
    }

    public function test_admin_role_can_create_new_companies()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $response = $this->post('/dashboard/companies', $this->testData);
        $company = Company::first();
        $response->assertRedirect('/dashboard/companies/' . $company->id);
    }
}
