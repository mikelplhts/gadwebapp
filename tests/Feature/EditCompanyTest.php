<?php

namespace Tests\Unit;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EditCompanyTest extends TestCase
{
    use RefreshDatabase;

    private array $testData = [
        'business_name' => 'Test Company',
        'address'       => 'Street Test 123',
        'zipcode'       => '12345',
        'city'          => 'Test City',
        'province'      => 'Test Province',
        'region'        => 'Test Region',
        'email'         => 'example@email.com'
    ];

    public function test_user_role_cannot_edit_companies()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $response = $this->put('dashboard/companies/' . $company->id, $this->testData);
        $response->assertStatus(403);
    }

    public function test_admin_role_can_edit_companies()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $response = $this->put('dashboard/companies/' . $company->id, $this->testData);
        $response->assertStatus(302);
    }
}
