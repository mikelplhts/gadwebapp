<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CompanyControllerTest extends TestCase
{
    use RefreshDatabase;
    
    // company index

    public function test_user_role_cannot_access_company_index()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        
        $response = $this->get('/dashboard/companies');
        $response->assertStatus(403);
    }

    public function test_admin_role_can_access_company_index()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        
        $response = $this->get('/dashboard/companies');
        $response->assertStatus(200);
    }

    // company create

    public function test_user_role_cannot_access_company_create()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/dashboard/companies/create');
        $response->assertStatus(403);
    }

    public function test_admin_role_can_access_company_create()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $response = $this->get('/dashboard/companies/create');
        $response->assertStatus(200);
    }

    // comapny show

    public function test_user_role_cannot_access_company_show()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $response = $this->get('/dashboard/companies/' . $company->id);
        $response->assertStatus(403);
    }

    public function test_admin_role_can_access_company_show()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $response = $this->get('/dashboard/companies/' . $company->id);
        $response->assertStatus(200);
    }

    // company edit

    public function test_user_role_cannot_access_company_edit()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $response = $this->get('/dashboard/companies/' . $company->id . '/edit');
        $response->assertStatus(403);
    }

    public function test_admin_role_can_access_company_edit()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $response = $this->get('/dashboard/companies/' . $company->id . '/edit');
        $response->assertStatus(200);
    }

    // company import

    public function test_user_role_cannot_access_company_import()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/dashboard/companies/import');
        $response->assertStatus(403);
    }

    public function test_admin_role_can_access_company_import()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $response = $this->get('/dashboard/companies/import');
        $response->assertStatus(200);
    }
}
