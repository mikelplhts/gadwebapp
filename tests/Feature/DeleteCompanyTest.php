<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteCompanyTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_role_cannot_delete_companies()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $this->delete('dashboard/companies/' . $company->id);
        $this->assertNotNull($company->fresh());
    }

    public function test_admin_role_can_edit_companies()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $company = Company::factory()->create();

        $this->delete('dashboard/companies/' . $company->id);
        $this->assertNull($company->fresh());
    }
}
