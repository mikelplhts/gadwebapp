<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Spatie\Permission\Models\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Creates an user with admin role.
     * 
     * @return User
     */
    protected function createAdminUser(): User
    {
        $adminRole = Role::findOrCreate('admin');

        $user = User::factory()->create();
        $user->assignRole($adminRole);

        return $user;
    }
}
