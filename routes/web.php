<?php

use App\Http\Controllers\CompanyController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified'])
    ->get('/', function () {
        return redirect()->route('dashboard');
    });

Route::middleware(['auth:sanctum', 'verified'])
    ->prefix('/dashboard')
    ->group(function () {
        // dashboard
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

        // companies
        Route::prefix('/companies')
            ->group(function () {
                Route::get('/', [CompanyController::class, 'index'])->name('company.index');
                Route::post('/', [CompanyController::class, 'store'])->name('company.store');
                Route::get('/import', [CompanyController::class, 'import'])->name('company.import');
                Route::post('/import', [CompanyController::class, 'storeImport'])->name('company.storeImport');
                Route::get('/create', [CompanyController::class, 'create'])->name('company.create');
                Route::get('/{id}', [CompanyController::class, 'show'])->name('company.show');
                Route::put('/{id}', [CompanyController::class, 'update'])->name('company.update');
                Route::delete('/{id}', [CompanyController::class, 'destroy'])->name('company.destroy');
                Route::get('/{id}/edit', [CompanyController::class, 'edit'])->name('company.edit');
            });
    });
